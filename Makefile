FORMATTER=luaformatter
FORMATTER_FLAGS=-a

CHECKER=luacheck
CHECKER_FLAGS=-g -u

SOURCES=main.lua index.lua next.lua

.PHONY: all format check

all: format check

format:
	$(FORMATTER) $(FORMATTER_FLAGS) $(SOURCES)

check:
	$(CHECKER) $(CHECKER_FLAGS) $(SOURCES)
