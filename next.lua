local composer = require("composer")
local widget = require("widget")

-- Set background to beige.
display.setDefault("background", 245 / 255, 245 / 255, 220 / 255)

local text = "Adipisci repudiandae ad nec, nec posse persecuti concludaturque ex. "
  .. "Vero prima cu mel. Est ignota equidem electram no, posse dolorem eam id. "
  .. "Eam liber animal adversarium et, nonumes aliquando mel ei, "
  .. "quis nostrud ceteros eam in. Velit epicurei disputationi ex pri. "
  .. "Et rebum instructior vel, brute nostrud ad pri."

local scene = composer.newScene()

-- create()
function scene:create( event )

  local sceneGroup = self.view
  -- Code here runs when the scene is first created but has not yet appeared on screen

  local content = display.newText(
    {
      text = text,
      x = display.contentWidth * 0.5,
      y = display.contentHeight * 0.4,
      width = display.contentWidth * 0.9,
      fontSize = 18,
    }
  )

  content:setFillColor(0 / 255, 0 / 255, 0 / 255)

  sceneGroup:insert(content)

  local btnPrev = widget.newButton(
    {
      x = display.contentWidth * 0.3,
      y = display.contentHeight * 0.85,
      label = "Previous",
      fontSize = 20,
      shape = "roundedRect",
      width = 100,
      labelColor = {
        default = { 0 / 255, 0 / 255, 0 / 255},
        over = { 0 / 255, 0 / 255, 0 / 255},
      },
      fillColor = {
        default = { 180 / 255, 255 / 255, 255 / 255},
        over = { 110 / 255, 255 / 255, 255 / 255},
      },
      onEvent = function (ev)
        composer.gotoScene("index")
      end
    }
  )

  sceneGroup:insert(btnPrev)

  local btnNext = widget.newButton(
    {
      x = display.contentWidth * 0.7,
      y = display.contentHeight * 0.85,
      label = "Next",
      fontSize = 20,
      shape = "roundedRect",
      width = 100,
      labelColor = {
        default = { 0 / 255, 0 / 255, 0 / 255},
        over = { 0 / 255, 0 / 255, 0 / 255},
      },
      fillColor = {
        default = { 180 / 255, 255 / 255, 255 / 255},
        over = { 110 / 255, 255 / 255, 255 / 255},
      },
      onEvent = function (ev)
        composer.gotoScene("final")
      end
    }
  )

  sceneGroup:insert(btnNext)
end


-- show()
function scene:show( event )

  local sceneGroup = self.view
  local phase = event.phase

  if ( phase == "will" ) then
    -- Code here runs when the scene is still off screen (but is about to come on screen)
    sceneGroup.isVisible = true
  elseif ( phase == "did" ) then
  -- Code here runs when the scene is entirely on screen

  end
end


-- hide()
function scene:hide( event )

  local sceneGroup = self.view
  local phase = event.phase

  if ( phase == "will" ) then
  -- Code here runs when the scene is on screen (but is about to go off screen)

  elseif ( phase == "did" ) then
    -- Code here runs immediately after the scene goes entirely off screen
    sceneGroup.isVisible = false
  end
end


-- destroy()
function scene:destroy( event )

  local sceneGroup = self.view
  -- Code here runs prior to the removal of scene's view

  sceneGroup:removeSelf()
end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
