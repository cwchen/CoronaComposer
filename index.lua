local composer = require("composer")
local widget = require("widget")

-- Set background to beige.
display.setDefault("background", 245 / 255, 245 / 255, 220 / 255)

local text = "Lorem ipsum dolor sit amet, vix ut persius laoreet consulatu, "
  .. "et populo equidem fastidii vel. Usu no scripta similique. "
  .. "Numquam fabellas his ex. Et ius dictas dolores forensibus, "
  .. "ea propriae salutatus sea. Per ei veri dicta conclusionemque, "
  .. "exerci eripuit per ex, tale appareat efficiendi nec et."

local scene = composer.newScene()

-- create()
function scene:create( event )

  local sceneGroup = self.view
  -- Code here runs when the scene is first created but has not yet appeared on screen

  local title = display.newText(
    {
      text = "My App",
      x = display.contentWidth * 0.5,
      y = display.contentHeight * 0.12,
      fontSize = 28,
    }
  )

  title:setFillColor(0 / 255, 0 / 255, 0 / 255)

  sceneGroup:insert(title)

  local content = display.newText(
    {
      text = text,
      x = display.contentWidth * 0.5,
      y = display.contentHeight * 0.45,
      width = display.contentWidth * 0.9,
      fontSize = 18,
    }
  )

  content:setFillColor(0 / 255, 0 / 255, 0 / 255)

  sceneGroup:insert(content)

  local btnNext = widget.newButton(
    {
      x = display.contentWidth * 0.5,
      y = display.contentHeight * 0.85,
      label = "Next",
      fontSize = 20,
      emboss = false,
      shape = "roundedRect",
      width = 100,
      labelColor = {
        default = { 0 / 255, 0 / 255, 0 / 255},
        over = { 0 / 255, 0 / 255, 0 / 255},
      },
      fillColor = {
        default = { 180 / 255, 255 / 255, 255 / 255},
        over = { 110 / 255, 255 / 255, 255 / 255},
      },
      onEvent = function (ev)
        composer.gotoScene("next")
      end
    }
  )

  sceneGroup:insert(btnNext)
end


-- show()
function scene:show( event )

  local sceneGroup = self.view
  local phase = event.phase

  if ( phase == "will" ) then
    -- Code here runs when the scene is still off screen (but is about to come on screen)
    sceneGroup.isVisible = true
  elseif ( phase == "did" ) then
  -- Code here runs when the scene is entirely on screen

  end
end


-- hide()
function scene:hide( event )

  local sceneGroup = self.view
  local phase = event.phase

  if ( phase == "will" ) then
  -- Code here runs when the scene is on screen (but is about to go off screen)

  elseif ( phase == "did" ) then
    -- Code here runs immediately after the scene goes entirely off screen
    sceneGroup.isVisible = false
  end
end


-- destroy()
function scene:destroy( event )

  local sceneGroup = self.view
  -- Code here runs prior to the removal of scene's view

  sceneGroup:removeSelf()
end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
